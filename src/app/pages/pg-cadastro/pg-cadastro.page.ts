import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, ToastController, IonSlides } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import { Usuario } from 'src/interfaces/usuario';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-pg-cadastro',
  templateUrl: './pg-cadastro.page.html',
  styleUrls: ['./pg-cadastro.page.scss'],
})
export class PgCadastroPage implements OnInit {
  @ViewChild(IonSlides,{static: false}) slides: IonSlides;
  public usuarioRegister: Usuario = {};
  public wavesPosition: number = 0;
  private wavesDifference: number = 100;
  public usuarioLogin: Usuario = {};
  private loading: any;

  constructor(
    private router: Router,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private afa: AngularFireAuth,
    private afs: AngularFirestore,
    private authService: AuthService
    ) { }

  ngOnInit() {
  }

  async cadastrar() {
    await this.presentLoading();
    try{
    const novoUsuario = await this.afa.auth.createUserWithEmailAndPassword(this.usuarioRegister.email, this.usuarioRegister.password)
    const novoUsuarioObjeto = Object.assign({}, this.usuarioRegister);
    novoUsuarioObjeto.usuarioId = novoUsuario.user.uid;
    delete novoUsuarioObjeto.email;
    delete novoUsuarioObjeto.password;

    this.afs.collection('Usuarios').doc(novoUsuario.user.uid).set(novoUsuarioObjeto).then((res) => {
      console.log("Usuário " + this.usuarioRegister.nome + " foi cadastrado com sucesso!");
      this.presentToast("Usuário " + this.usuarioRegister.nome + " foi cadastrado com sucesso!");
      this.loading.dismiss();
      this.navCtrl.navigateBack('/pg-login');
      }).catch((err) => {
        this.loading.dismiss();
        console.log(err);
        console.log(err.code);
        console.log(err.message);
        const code = err.code;
        let message = 'Não foi possivel cadastar você, por favor tente novamente.';
        if (code === 'auth/email-already-exists') {
          message = 'Este endereço de email já existe!';
        } else if (code === 'auth/invalid-email') {
          message = 'O endereço de email não pode ser encontrado.'
        } else if (code === 'auth/invalid-password') {
          message = 'A senha é inválida.'
        } else if (code === 'auth/user-not-found'){
          message = 'Usuário não encontrado.'
        } else if (code === 'auth/wrong-password') {
        message = 'A senha esta incorreta.'
      }
        this.presentToast(message);
      });
    }catch(err){
      this.loading.dismiss();
      console.log(err);
      console.log(err.code);
      console.log(err.message);
      const code = err.code;
      let message = 'Não foi possivel cadastar você, por favor tente novamente.';
      if (code === 'auth/email-already-exists') {
        message = 'Este endereço de email já existe.';
      } else if (code === 'auth/invalid-email') {
        message = 'O endereço de email não pode ser encontrado.'
      } else if (code === 'auth/invalid-password') {
        message = 'A senha é inválida.'
      } else if (code === 'auth/user-not-found') {
        message = 'Usuário não encontrado.'
      } else if (code === 'auth/wrong-password') {
        message = 'A senha esta incorreta.'
      } else  if (code === 'auth/email-already-in-use') {
        message = 'Este endereço de email já esta em uso.';
      }
      this.presentToast(message);
    }
  }

  segmentChanged(event: any) {
    if (event.detail.value === 'login') {
      this.slides.slidePrev();
      this.wavesPosition += this.wavesDifference;
    } else {
      this.slides.slideNext();
      this.wavesPosition -= this.wavesDifference;
    }
  }
  
  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }


  goToLogin(){
    this.navCtrl.navigateForward('/pg-login');
  }

}
