import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, MenuController, LoadingController, ToastController, IonSlides, Events } from '@ionic/angular';
import { Usuario } from 'src/interfaces/usuario';

import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import { AuthService } from 'src/services/auth.service';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/services/usuario.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-pg-login',
  templateUrl: './pg-login.page.html',
  styleUrls: ['./pg-login.page.scss'],
})
export class PgLoginPage implements OnInit {
  @ViewChild(IonSlides,{static: false}) slides: IonSlides;
  public wavesPosition: number = 0;
  private wavesDifference: number = 100;
  public usuarioLogin: Usuario = {};
  private loading: any;

  constructor(
    private usuarioService: UsuarioService,
    private navCtrl: NavController,
    private menuCtrl: MenuController,
    private afa: AngularFireAuth,
    private afs: AngularFirestore,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private authService: AuthService,
    private router: Router,
    private events: Events,
    ) { 
      this.menuCtrl.enable(false);
    }

    ngOnInit() {
    }

  async login(){
    await this.presentLoading();
    this.authService.login(this.usuarioLogin).then((res) => {
      this.loading.dismiss();
      this.router.navigateByUrl('/pg-tabs');
    })
      .catch((err) => {
        this.loading.dismiss();
        console.log(err);
        console.log(err.code);
        console.log(err.message);
        const code = err.code;
        let message = 'Não foi possivel logar você, por favor tente novamente.';
        if (code === 'auth/email-already-exists') {
          message = 'Este endereço de email já existe.';
        } else if (code === 'auth/invalid-email') {
          message = 'O endereço de email não pode ser encontrado.'
        } else if (code === 'auth/invalid-password') {
          message = 'A senha é inválida.'
        } else if (code === 'auth/user-not-found'){
          message = 'Usuário não encontrado.'
        } else if (code === 'auth/wrong-password') {
        message = 'A senha esta incorreta.'
        } else  if (code === 'auth/email-already-in-use') {
          message = 'Este endereço de email já esta em uso.';
        }
        this.presentToast(message);
      });
  }

  ionViewWillEnter() {
    //Desaabilita o Side-menu enquanto o login não for efetuado.
    this.menuCtrl.enable(false);
  }

  goToCadastro(){
    this.navCtrl.navigateForward('/pg-cadastro');
  }

  segmentChanged(event: any) {
    if (event.detail.value === 'login') {
      this.slides.slidePrev();
      this.wavesPosition += this.wavesDifference;
    } else {
      this.slides.slideNext();
      this.wavesPosition -= this.wavesDifference;
    }
  }
  
  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }

}
