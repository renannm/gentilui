import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Usuario } from 'src/interfaces/usuario';
import { UsuarioService } from 'src/services/usuario.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-pg-editar-professores',
  templateUrl: './pg-editar-professores.page.html',
  styleUrls: ['./pg-editar-professores.page.scss'],
})
export class PgEditarProfessoresPage implements OnInit {
  public usuarios = new Array<Usuario>();
  private usuariosSubscription: Subscription;
  public usuarioId: string = null;
  private loading: any;
  

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private authService: AuthService,
    private usuarioService: UsuarioService,
    private router: Router, 
    private navCtrl: NavController) {
      this.usuariosSubscription = this.usuarioService.getUsuarios().subscribe(data => {
        this.usuarios = data;
      });

      this.usuarioId = this.authService.getAuth().currentUser.uid;
     }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.usuariosSubscription.unsubscribe();
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Excluindo...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }

  }

 

