import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PgEditarProfessoresPage } from './pg-editar-professores.page';

describe('PgEditarProfessoresPage', () => {
  let component: PgEditarProfessoresPage;
  let fixture: ComponentFixture<PgEditarProfessoresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PgEditarProfessoresPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PgEditarProfessoresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
