import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PgEditarProfessoresPage } from './pg-editar-professores.page';

const routes: Routes = [
  {
    path: '',
    component: PgEditarProfessoresPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PgEditarProfessoresPage]
})
export class PgEditarProfessoresPageModule {}
