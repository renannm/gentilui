import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Usuario } from 'src/interfaces/usuario';
import { UsuarioService } from 'src/services/usuario.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-pg-editar-alunos',
  templateUrl: './pg-editar-alunos.page.html',
  styleUrls: ['./pg-editar-alunos.page.scss'],
})
export class PgEditarAlunosPage implements OnInit {
  public usuarios = new Array<Usuario>();
  private usuariosSubscription: Subscription;
  public usuarioId: string = null;
  private loading: any;
  

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private authService: AuthService,
    private usuarioService: UsuarioService,
    private router: Router, 
    private navCtrl: NavController) {
      this.usuariosSubscription = this.usuarioService.getUsuarios().subscribe(data => {
        this.usuarios = data;
      });

      this.usuarioId = this.authService.getAuth().currentUser.uid;
     }

  ngOnInit() {
  }

  async deletarUsuarios(id:string){
    await this.presentLoading();
    try {
      await this.usuarioService.deleteUsuario(id);
      await this.loading.dismiss();
      this.presentToast('Usuário excluído com sucesso!');

      console.log("Usuário excluído com sucesso!");
      this.navCtrl.navigateBack('/pg-editar-professores');

    } catch (error) {
      console.log(error);
      this.presentToast('Erro ao tentar excluir');
      this.loading.dismiss();
    }
  }

  ngOnDestroy() {
    this.usuariosSubscription.unsubscribe();
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Excluindo...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }
}
