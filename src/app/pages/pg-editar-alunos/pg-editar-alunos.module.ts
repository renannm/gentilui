import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PgEditarAlunosPage } from './pg-editar-alunos.page';

const routes: Routes = [
  {
    path: '',
    component: PgEditarAlunosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PgEditarAlunosPage]
})
export class PgEditarAlunosPageModule {}
