import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PgEditarAlunosPage } from './pg-editar-alunos.page';

describe('PgEditarAlunosPage', () => {
  let component: PgEditarAlunosPage;
  let fixture: ComponentFixture<PgEditarAlunosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PgEditarAlunosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PgEditarAlunosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
