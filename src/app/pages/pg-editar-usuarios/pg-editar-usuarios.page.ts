import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/interfaces/usuario';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { AuthService } from 'src/services/auth.service';
import { UsuarioService } from 'src/services/usuario.service';


@Component({
  selector: 'app-pg-editar-usuarios',
  templateUrl: './pg-editar-usuarios.page.html',
  styleUrls: ['./pg-editar-usuarios.page.scss'],
})
export class PgEditarUsuariosPage implements OnInit {
  public  usuario: Usuario = {};
  private loading: any;
  private usuarioId: string = null;
  private usuarioSubscription: Subscription;

  constructor(
    private router: Router, 
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private authService: AuthService,
    private activeRoute: ActivatedRoute,
    private usuarioService: UsuarioService
  ) { 
    this.usuarioId = this.activeRoute.snapshot.params['id'];
    if (this.usuarioId) this.loadAtividade();
  }

  loadAtividade(){
    this.usuarioSubscription = this.usuarioService.getUsuario(this.usuarioId).subscribe(data =>{
      this.usuario = data;
    });
  }

  ngOnDestroy() {
    if (this.usuarioSubscription) this.usuarioSubscription.unsubscribe();
  }

  async salvarUsuario(){
    await this.presentLoading();

    if(this.usuarioId){
      try{
        await this.usuarioService.updateUsuario(this.usuarioId, this.usuario);
        await this.loading.dismiss();
        this.presentToast("Alterações salvas com sucesso!");

        this.navCtrl.pop();
      }catch (error){
        this.presentToast('Erro ao tentar salvar');
        this.loading.dismiss();
      }
    }
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Editando...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }


  ngOnInit() {
  }

}
