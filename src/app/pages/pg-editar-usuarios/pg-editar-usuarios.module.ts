import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PgEditarUsuariosPage } from './pg-editar-usuarios.page';

const routes: Routes = [
  {
    path: '',
    component: PgEditarUsuariosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PgEditarUsuariosPage]
})
export class PgEditarUsuariosPageModule {}
