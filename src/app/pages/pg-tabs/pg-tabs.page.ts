import { Component, OnInit,ViewChildren } from '@angular/core';
import { IonContent, NavController, MenuController} from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pg-tabs',
  templateUrl: './pg-tabs.page.html',
  styleUrls: ['./pg-tabs.page.scss'],
})
export class PgTabsPage implements OnInit {
  @ViewChildren(IonContent) content: IonContent;

  ionViewWillEnter() {
    //Habilita o Side-menu após o login com sucesso.
    this.menuCtrl.enable(true);
  }

  constructor(private router: Router, 
    private navCtrl: NavController,
    private menuCtrl: MenuController) { }

  // Não Usado
  goToNovosPage(){
    this.router.navigateByUrl('/pg-novos-page');
  }
  // Não Usado
  goToAntigosPage(){
    this.router.navigateByUrl('/pg-antigos-page');
  }

  goToNovaAtividade(){
    this.navCtrl.navigateForward('/pg-nova-atividade');
  }

  ngOnInit() {
  }

  // Não usado
  scrollToTop() {
    this.content.scrollToTop(400);
    console.log("Executado o Scroll to Top");
  }
}
