import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PgNovosPage } from './pg-novos.page';

describe('PgNovosPage', () => {
  let component: PgNovosPage;
  let fixture: ComponentFixture<PgNovosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PgNovosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PgNovosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
