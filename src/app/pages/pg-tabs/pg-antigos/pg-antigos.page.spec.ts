import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PgAntigosPage } from './pg-antigos.page';

describe('PgAntigosPage', () => {
  let component: PgAntigosPage;
  let fixture: ComponentFixture<PgAntigosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PgAntigosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PgAntigosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
