import { Component, OnInit, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, IonContent } from '@ionic/angular';
import { AtividadeService } from 'src/services/atividade.service';
import { Atividade } from 'src/interfaces/atividades';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-pg-antigos',
  templateUrl: './pg-antigos.page.html',
  styleUrls: ['./pg-antigos.page.scss'],
  providers: [DatePipe]
})
export class PgAntigosPage implements OnInit {
  @ViewChildren(IonContent) content: IonContent;
  public atividades = new Array<Atividade>();
  private atividadesSubscription: Subscription;
  private dataAtual: any;
  private horaAtual: any;
  private dataDeFora: any = null;
  private resultado: any = null;

  constructor(
    private datePipe: DatePipe,
    private atividadeService: AtividadeService,
    private router: Router,
    private navCtrl: NavController) {

    //Criação da data que servirá de base (ou pivô) para 
    //a definição de atividades como Novas ou Antigas
    this.dataAtual = new Date();
    //Realizada a conversão da Data em Milissegundos,
    //retornando uma única String
    Date.parse(this.dataAtual);

    // console.log(Date.parse(this.dataAtual));

    this.atividadesSubscription = this.atividadeService.getAtividades().subscribe(data => {
      this.atividades = data;
    });
  }

  ngOnInit() {
  }

  testeAntigo(data: string) {
    //Criada uma data específica do formato Date 
    //utilizando a string recebida pelo método
    this.dataDeFora = new Date(data);

      //Realizada a conversão da Data em Milissegundos,
    //retornando uma única String.
    //Facilitando assim comparações e sendo o mais preciso possível.
    this.resultado = Date.parse(this.dataDeFora);

    // console.log(Date.parse(this.resultado));

    //Por fim, comparamos a data agora convertida em Milisegundos 
    //com a Data Pivô criada no construtor, retornando verdadeiro ou falso.
    //Decidindo se anotícia será exibida ou não na página atual
    if (this.dataDeFora < this.dataAtual) {
      // console.log("testeAntigo TRUE");
      return true;
    } else {
      // console.log("testeAntigo FALSE");
      return false;
    }
  }

}
