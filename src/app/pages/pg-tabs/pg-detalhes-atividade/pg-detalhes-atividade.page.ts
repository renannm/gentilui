import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Atividade } from 'src/interfaces/atividades';
import { AuthService } from 'src/services/auth.service';
import { Subscription } from 'rxjs';
import { AtividadeService } from 'src/services/atividade.service';


@Component({
  selector: 'app-pg-detalhes-atividade',
  templateUrl: './pg-detalhes-atividade.page.html',
  styleUrls: ['./pg-detalhes-atividade.page.scss'],
})
export class PgDetalhesAtividadePage implements OnInit {
    public  atividade: Atividade = {};
    private loading: any;
    private atividadeId: string = null;
    private atividadeSubscription:Subscription;

  constructor(
    private activeRoute: ActivatedRoute,
    private atividadeService: AtividadeService) {
      this.atividadeId = this.activeRoute.snapshot.params['id'];

      if (this.atividadeId) this.loadAtividade();
     }

    ngOnInit() {
    }

    ngOnDestroy() {
      if (this.atividadeSubscription) this.atividadeSubscription.unsubscribe();
    }

    loadAtividade(){
      this.atividadeSubscription = this.atividadeService.getAtividade(this.atividadeId).subscribe(data =>{
        this.atividade = data;
      });
    }

}
