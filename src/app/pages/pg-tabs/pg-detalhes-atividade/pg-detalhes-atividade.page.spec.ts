import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PgDetalhesAtividadePage } from './pg-detalhes-atividade.page';

describe('PgDetalhesAtividadePage', () => {
  let component: PgDetalhesAtividadePage;
  let fixture: ComponentFixture<PgDetalhesAtividadePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PgDetalhesAtividadePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PgDetalhesAtividadePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
