import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PgTabsPage } from './pg-tabs.page';

// Comecei a usar o caminho absoluto dos módulos após um erro ao clonar o repo em outra máquina.
const routes: Routes = [
  {
    path: '',
    component: PgTabsPage,
    children:[
      { path: 'pg-novos', loadChildren: 'src/app/pages/pg-tabs/pg-novos/pg-novos.module#PgNovosPageModule'},
      { path: 'pg-antigos', loadChildren: 'src/app/pages/pg-tabs/pg-antigos/pg-antigos.module#PgAntigosPageModule'},
      { path: '', loadChildren: 'src/app/pages/pg-tabs/pg-novos/pg-novos.module#PgNovosPageModule', pathMatch: 'full'},
    ]
  }
]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PgTabsPage]
})
export class PgTabsPageModule {}
