import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PgTabsPage } from './pg-tabs.page';

describe('PgTabsPage', () => {
  let component: PgTabsPage;
  let fixture: ComponentFixture<PgTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PgTabsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PgTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
