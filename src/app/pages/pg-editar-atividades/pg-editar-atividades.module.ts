import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PgEditarAtividadesPage } from './pg-editar-atividades.page';

const routes: Routes = [
  {
    path: '',
    component: PgEditarAtividadesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PgEditarAtividadesPage]
})
export class PgEditarAtividadesPageModule {}
