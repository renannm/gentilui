import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PgEditarAtividadesPage } from './pg-editar-atividades.page';

describe('PgEditarAtividadesPage', () => {
  let component: PgEditarAtividadesPage;
  let fixture: ComponentFixture<PgEditarAtividadesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PgEditarAtividadesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PgEditarAtividadesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
