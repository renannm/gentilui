import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Atividade } from 'src/interfaces/atividades';
import { Subscription } from 'rxjs';
import { AtividadeService } from 'src/services/atividade.service';
import { AuthService } from 'src/services/auth.service';
import { Usuario } from 'src/interfaces/usuario';
import { UsuarioService } from 'src/services/usuario.service';

@Component({
  selector: 'app-pg-editar-atividades',
  templateUrl: './pg-editar-atividades.page.html',
  styleUrls: ['./pg-editar-atividades.page.scss'],
})
export class PgEditarAtividadesPage implements OnInit {
  public atividades = new Array<Atividade>();
  private atividadesSubscription: Subscription;
  private atividadeId: string = null;
  private loading: any;


  constructor(
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private atividadeService: AtividadeService,
    private navCtrl: NavController,
    private activeRoute: ActivatedRoute) {

    this.atividadeId = this.activeRoute.snapshot.params['id'];

    this.atividadesSubscription = this.atividadeService.getAtividades().subscribe(data => {
      this.atividades = data;
    });
  }

  async deletarAtividade(id: string) {
    await this.presentLoading();
    try {
      await this.atividadeService.deleteAtividade(id);
      await this.loading.dismiss();
      this.presentToast('Atividade excluída com sucesso!');

      console.log("Atividade excluída com sucesso!");
      this.navCtrl.navigateBack('/pg-editar-atividades');

    } catch (error) {
      console.log(error);
      this.presentToast('Erro ao tentar excluir');
      this.loading.dismiss();
    }
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.atividadesSubscription.unsubscribe();
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Excluindo...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }

}
