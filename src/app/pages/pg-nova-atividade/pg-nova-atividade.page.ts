import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, ToastController, AlertController } from '@ionic/angular';
import { AtividadeService } from 'src/services/atividade.service';
import { Atividade } from 'src/interfaces/atividades';
import { AuthService } from 'src/services/auth.service';
import { DatePipe, getLocaleDateFormat } from '@angular/common';
import { Plugins, Capacitor, CameraResultType, CameraSource, FilesystemDirectory } from '@capacitor/core';
import { switchMap, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Platform } from '@ionic/angular';
import { storage } from 'firebase';

@Component({
  selector: 'app-pg-nova-atividade',
  templateUrl: './pg-nova-atividade.page.html',
  styleUrls: ['./pg-nova-atividade.page.scss'],
  providers: [DatePipe]
})
export class PgNovaAtividadePage implements OnInit {
  public permicaoLocalizacao = false;
  public atividade: Atividade = {};
  private loading: any;
  lat: number;
  long: number;
  private endereco: string = null;
  imagem: SafeResourceUrl;
  caminhoDaFotoCamera: any;
  caminhoDaFotoGaleria: any;
  public opcaoDeFoto: string = null;
  tipoDeDispositivo = false;

  constructor(
    private plataform: Platform,
    private http: HttpClient,
    private datePipe: DatePipe,
    private atividadeService: AtividadeService,
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private router: Router,
    private navCtrl: NavController,
    private alertCtrl: AlertController) { }

  ngOnInit() {
    this.getPosicao();
    
    //Código para checar se o dispositivo é um Desktop
    //Se sim, os botões para informar imagens 
    // pela Cãmera e Galeria é ocultado para evitar erros.
    if (this.plataform.is("desktop")) {
      this.tipoDeDispositivo = true;
    } else {
      console.log(this.plataform.platforms());
    }
  }
  //METODO PARA ESCOLHER FOTO DA GALERIA
  async escolherFoto() {
    this.opcaoDeFoto = "galeria";
    const { Camera, Filesystem } = Plugins;
    //Constante com as opções fornecidas ao usuário, além de como será salva a imagem recebida (URI)
    const options = { 
      quality: 75,
      allowEditing: false,
      source: CameraSource.Photos, //esse é o campo que define se é da Galeria ou da Câmera (nesse caso é galeria)
      resultType: CameraResultType.Uri
    };

    const fotoOriginal = await Camera.getPhoto(options); //Foto original snedo obtida
    const fotoInTempStorage = await Filesystem.readFile({ path: fotoOriginal.path }); //Foto sendo copiada temporáriamente para o storage

    let date = new Date(), //Obtendo uma nova data atual para representar quando foi adicionada ao sistema
      time = date.getTime(),
      fileName = time + ".jpeg";

    await Filesystem.writeFile({ //Realizando o salvamento da cópia no Storage do Capacitor
      data: fotoInTempStorage.data,
      path: fileName,
      directory: FilesystemDirectory.Data
    });

    const finalFotoUri = await Filesystem.getUri({ //Por fim, é gerada a URI da imagem salva no Storage do Capacitor
      directory: FilesystemDirectory.Data,
      path: fileName
    });

    this.caminhoDaFotoGaleria = Capacitor.convertFileSrc(finalFotoUri.uri);
    console.log(this.caminhoDaFotoGaleria);
  }

//METODO PARA ESCOLHER FOTO DA CAMERA
async tirarFoto() {
  try{
  this.opcaoDeFoto = "foto";
  const { Camera, Filesystem } = Plugins;
  //Constante com as opções fornecidas ao usuário, além de como será salva a imagem recebida (URI)
  const options = { 
    quality: 50,
    allowEditing: false,
    source: CameraSource.Camera, //esse é o campo que define se é da Galeria ou da Câmera
    resultType: CameraResultType.DataUrl,
  };

  const fotoOriginal = await Camera.getPhoto(options); //Foto original snedo obtida

  const image = 'data:image/jpeg; base64, ${result}';

  const pictures = storage().ref('pictures');
  
  pictures.putString(image,'data_url');
  
  } catch (error){
    console.log(error);
  }
} 



  //   const fotoInTempStorage = await Filesystem.readFile({ path: fotoOriginal.path }); //Foto sendo copiada temporáriamente para o storage

//   let date = new Date(), //Obtendo uma nova data atual para representar quando foi adicionada ao sistema
//     time = date.getTime(),
//     fileName = time + ".jpeg";

//   await Filesystem.writeFile({ //Realizando o salvamento da cópia no Storage do Capacitor
//     data: fotoInTempStorage.data,
//     path: fileName,
//     directory: FilesystemDirectory.Data
//   });

//   const finalFotoUri = await Filesystem.getUri({ //Por fim, é gerada a URI da imagem salva no Storage do Capacitor
//     directory: FilesystemDirectory.Data,
//     path: fileName
//   });

//   this.caminhoDaFotoCamera = Capacitor.convertFileSrc(finalFotoUri.uri);
//   console.log(this.caminhoDaFotoCamera);
// }


  permitirLocalizacao(){
   this.permicaoLocalizacao = true;
   this.showEndereco();
  }

  //IMPLEMENTANDO A PEGADA DA LOCALIZAÇÃO
  getPosicao() {
    if (!Capacitor.isPluginAvailable('Geolocation')) {
      this.mostrarAlertaDeErro();
      return;
    }
    Plugins.Geolocation.getCurrentPosition()
      .then(resultado => {
        this.lat = resultado.coords.latitude;
        this.long = resultado.coords.longitude;
  
        console.log(this.lat, this.long);
        // chamando a função getEndereço para decodificar o endereço 

        this.getEndereco(this.lat, this.long).subscribe(enderecoDecodificado => {
          this.endereco = enderecoDecodificado;
          console.log(this.endereco);
        });
      })
      .catch(err => {
        this.mostrarAlertaDeErro();
      });
  }

  // Esta função faz uma chamada http para a api do google para decodificar as coordenadas
  private getEndereco(lat: number, long: number){
    return this.http
    .get<any>(
      `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${long}&key=${
          environment.googleMapsAPIKey
        }`
    ).pipe(
      map( geoData =>{
        if( !geoData || !geoData.results || geoData.results.lenght === 0) {
          return null;
        }
        return geoData.results[0].formatted_address;
      })
    );
  }

  async showEndereco(){
    await console.log(this.endereco);
  }

  // função para mostrar o toast com a localização e botão de dismissfunction to display the toast with location and dismiss button

  async presentToast2() {
    const toast = await this.toastCtrl.create({
      message: this.endereco,

      position: 'middle',
      buttons: [
        {
          icon: 'close-circle',
          role: 'cancel'
        }
      ]
    });
    toast.present();
  }

  // Clique na função para mostar uma mensagem via toast com o endereço

  onMarkerClick() {
    this.presentToast2();
  }

  //IMPLEMENTANDO O CADASTRO DE ATIVIDADES
  async cadastrarAtividade() {
    await this.presentLoading();
    this.atividade.usuarioId = this.authService.getAuth().currentUser.uid;
    this.atividade.localizacao = this.endereco;

    //Checa se a última imagem forncedida foi da Camera ou Galeria, e pega a pultima e cadastra no banco.
    if(this.opcaoDeFoto === 'galeria'){
      this.atividade.imagemUrl = this.caminhoDaFotoGaleria;
    }else if(this.opcaoDeFoto === 'foto'){
      this.atividade.imagemUrl = this.caminhoDaFotoCamera;
    }

    //Caso nenhuma imagem tenha sido fornecida, é adicionada uma imagem de placeholder
    if(!this.atividade.imagemUrl){
      this.atividade.imagemUrl = 'https://cdn.pixabay.com/photo/2016/06/07/16/12/sky-1441936_960_720.jpg'
    };

    try {
      await this.atividadeService.addAtividade(this.atividade);
      await this.loading.dismiss();
      this.navCtrl.navigateBack('/pg-tabs');
      this.presentToast('Atividade criada com sucesso!');
    } catch (error) {
      this.presentToast('Erro ao tentar salvar');
      console.log(error);
      this.loading.dismiss();
    }
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Criando...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }

  mostrarAlertaDeErro() {
    this.alertCtrl.create({
      header: 'Erro ao buscar localização',
      message: 'Não foi possível obter sua localização.',
    }).then(alertEl => alertEl.present());
  }

}


