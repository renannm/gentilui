import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AgmCoreModule } from '@agm/core';

import { IonicModule } from '@ionic/angular';

import { PgNovaAtividadePage } from './pg-nova-atividade.page';
import { environment } from 'src/environments/environment';

const routes: Routes = [
  {
    path: '',
    component: PgNovaAtividadePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsAPIKey
    })
  ],
  declarations: [PgNovaAtividadePage]
})
export class PgNovaAtividadePageModule {}
