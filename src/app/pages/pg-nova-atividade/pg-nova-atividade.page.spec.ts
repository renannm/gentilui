import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PgNovaAtividadePage } from './pg-nova-atividade.page';

describe('PgNovaAtividadePage', () => {
  let component: PgNovaAtividadePage;
  let fixture: ComponentFixture<PgNovaAtividadePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PgNovaAtividadePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PgNovaAtividadePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
