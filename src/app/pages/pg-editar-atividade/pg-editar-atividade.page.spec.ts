import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PgEditarAtividadePage } from './pg-editar-atividade.page';

describe('PgEditarAtividadePage', () => {
  let component: PgEditarAtividadePage;
  let fixture: ComponentFixture<PgEditarAtividadePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PgEditarAtividadePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PgEditarAtividadePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
