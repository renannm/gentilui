import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PgEditarAtividadePage } from './pg-editar-atividade.page';

const routes: Routes = [
  {
    path: '',
    component: PgEditarAtividadePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PgEditarAtividadePage]
})
export class PgEditarAtividadePageModule {}
