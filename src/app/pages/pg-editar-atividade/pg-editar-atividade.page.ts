import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Atividade } from 'src/interfaces/atividades';
import { AuthService } from 'src/services/auth.service';
import { Subscription } from 'rxjs';
import { AtividadeService } from 'src/services/atividade.service';
import { HttpClient } from '@angular/common/http';
import { Plugins, Capacitor, CameraResultType, CameraSource, FilesystemDirectory } from '@capacitor/core';
import { Platform } from '@ionic/angular';


@Component({
  selector: 'app-pg-editar-atividade',
  templateUrl: './pg-editar-atividade.page.html',
  styleUrls: ['./pg-editar-atividade.page.scss'],
})
export class PgEditarAtividadePage implements OnInit {
    public  atividade: Atividade = {};
    private loading: any;
    private atividadeId: string = null;
    private atividadeSubscription: Subscription;
    caminhoDaFotoCamera: any;
    caminhoDaFotoGaleria: any;
    public opcaoDeFoto: string = null;
    tipoDeDispositivo = false;

  constructor(
    private plataform: Platform,
    private http: HttpClient,
    private router: Router,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private authService: AuthService,
    private activeRoute: ActivatedRoute,
    private atividadeService: AtividadeService
    ) {
    //Código para checar se o dispositivo é um Desktop
    //Se sim, os botões para informar imagens 
    // pela Cãmera e Galeria é ocultado para evitar erros.
    if (this.plataform.is("desktop")) {
      this.tipoDeDispositivo = true;
    } else {
      console.log(this.plataform.platforms());
    }

    //Pegando o ID da atividade e carregando os dados da atividade
      this.atividadeId = this.activeRoute.snapshot.params['id'];
      if (this.atividadeId) this.loadAtividade();
     }

    ngOnInit() {
    }

    ngOnDestroy() {
      if (this.atividadeSubscription) this.atividadeSubscription.unsubscribe();
    }

    loadAtividade(){
      this.atividadeSubscription = this.atividadeService.getAtividade(this.atividadeId).subscribe(data =>{
        this.atividade = data;
      });
    }

    async salvarAtividade(){
      await this.presentLoading();
      this.atividade.usuarioId = this.authService.getAuth().currentUser.uid;
       
      //Checa se a última imagem forncedida foi da Camera ou Galeria, e pega a pultima e cadastra no banco.
      if (this.opcaoDeFoto === 'galeria') {
        this.atividade.imagemUrl = this.caminhoDaFotoGaleria;
      } else if (this.opcaoDeFoto === 'foto') {
        this.atividade.imagemUrl = this.caminhoDaFotoCamera;
      }

      //Caso nenhuma imagem tenha sido fornecida, é adicionada uma imagem de placeholder
      if (!this.atividade.imagemUrl) {
        this.atividade.imagemUrl = 'https://cdn.pixabay.com/photo/2016/06/07/16/12/sky-1441936_960_720.jpg'
      };

      if(this.atividadeId){
        try{
          await this.atividadeService.updateAtividade(this.atividadeId, this.atividade);
          await this.loading.dismiss();
          this.presentToast("Alterações salvas com sucesso!");

          this.navCtrl.navigateBack('/pg-editar-atividades');
        }catch (error){
          this.presentToast('Erro ao tentar salvar');
          this.loading.dismiss();
        }
      }
    }

    async presentLoading() {
      this.loading = await this.loadingCtrl.create({ message: 'Editando...' });
      return this.loading.present();
    }
  
    async presentToast(message: string) {
      const toast = await this.toastCtrl.create({ message, duration: 2000 });
      toast.present();
    }

     //METODO PARA ESCOLHER FOTO DA GALERIA
  async escolherFoto() {
    this.opcaoDeFoto = "galeria";
    const { Camera, Filesystem } = Plugins;
    //Constante com as opções fornecidas ao usuário, além de como será salva a imagem recebida (URI)
    const options = { 
      quality: 75,
      allowEditing: false,
      source: CameraSource.Photos, //esse é o campo que define se é da Galeria ou da Câmera (nesse caso é galeria)
      resultType: CameraResultType.Uri
    };

    const fotoOriginal = await Camera.getPhoto(options); //Foto original snedo obtida
    const fotoInTempStorage = await Filesystem.readFile({ path: fotoOriginal.path }); //Foto sendo copiada temporáriamente para o storage

    let date = new Date(), //Obtendo uma nova data atual para representar quando foi adicionada ao sistema
      time = date.getTime(),
      fileName = time + ".jpeg";

    await Filesystem.writeFile({ //Realizando o salvamento da cópia no Storage do Capacitor
      data: fotoInTempStorage.data,
      path: fileName,
      directory: FilesystemDirectory.Data
    });

    const finalFotoUri = await Filesystem.getUri({ //Por fim, é gerada a URI da imagem salva no Storage do Capacitor
      directory: FilesystemDirectory.Data,
      path: fileName
    });

    this.caminhoDaFotoGaleria = Capacitor.convertFileSrc(finalFotoUri.uri);
    console.log(this.caminhoDaFotoGaleria);
  }

//METODO PARA ESCOLHER FOTO DA CAMERA
async tirarFoto() {
  this.opcaoDeFoto = "foto";
  const { Camera, Filesystem } = Plugins;
  //Constante com as opções fornecidas ao usuário, além de como será salva a imagem recebida (URI)
  const options = { 
    quality: 75,
    allowEditing: false,
    source: CameraSource.Camera, //esse é o campo que define se é da Galeria ou da Câmera
    resultType: CameraResultType.Uri,

  };

  const fotoOriginal = await Camera.getPhoto(options); //Foto original snedo obtida
  const fotoInTempStorage = await Filesystem.readFile({ path: fotoOriginal.path }); //Foto sendo copiada temporáriamente para o storage

  let date = new Date(), //Obtendo uma nova data atual para representar quando foi adicionada ao sistema
    time = date.getTime(),
    fileName = time + ".jpeg";

  await Filesystem.writeFile({ //Realizando o salvamento da cópia no Storage do Capacitor
    data: fotoInTempStorage.data,
    path: fileName,
    directory: FilesystemDirectory.Data
  });

  const finalFotoUri = await Filesystem.getUri({ //Por fim, é gerada a URI da imagem salva no Storage do Capacitor
    directory: FilesystemDirectory.Data,
    path: fileName
  });

  this.caminhoDaFotoCamera = Capacitor.convertFileSrc(finalFotoUri.uri);
  console.log(this.caminhoDaFotoCamera);
}

}

