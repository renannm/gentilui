import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Platform, Events } from '@ionic/angular';
// import { SplashScreen } from '@ionic-native/splash-screen/ngx';
// import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { Usuario } from 'src/interfaces/usuario';
import { Subscription } from 'rxjs';
import { UsuarioService } from 'src/services/usuario.service';

import { Plugins, Capacitor } from '@capacitor/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public usuarios: Usuario ={};
  public usuarioId: string = null;
  private usuarioSubscription: Subscription;

  constructor(
    private platform: Platform,
    // private splashScreen: SplashScreen,
    // private statusBar: StatusBar,
    private authService: AuthService,
    private usuarioService: UsuarioService,
    private router: Router,
    private http: HttpClient,
    private events: Events,
  ) {
    this.initializeApp();

  //   events.subscribe('usuario:logado', () =>{
  //   this.usuarioId = this.authService.getAuth().currentUser.uid;
  //   this.usuarioSubscription = this.usuarioService.getUsuario(this.usuarioId).subscribe(data =>{
  //   this.usuarios = data; });
  //   console.log(this.usuarios.nome);
  //   });
  }

  ngOnDestroy() {
    this.usuarioSubscription.unsubscribe();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if(Capacitor.isPluginAvailable('SplashScreen')){
        Plugins.SplashScreen.hide();
      }
      // this.statusBar.styleDefault();
      // this.splashScreen.hide();
    });
  }
  onLogout(){
    this.authService.logout();
    console.log('Logout realizado com sucesso!');
    this.router.navigateByUrl('/pg-login');
  }
}
