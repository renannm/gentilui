import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';


const routes: Routes = [
  { path: '', redirectTo: 'pg-login', pathMatch: 'full'},
  { path: 'pg-login', loadChildren:'./pages/pg-login/pg-login.module#PgLoginPageModule',canActivate:[LoginGuard]},
  { path: 'pg-tabs', loadChildren:'./pages/pg-tabs/pg-tabs.module#PgTabsPageModule', canActivate:[AuthGuard]},
  { path: 'pg-cadastro', loadChildren:'./pages/pg-cadastro/pg-cadastro.module#PgCadastroPageModule'}, 
  { path: 'pg-novos', loadChildren: './pages/pg-tabs/pg-novos/pg-novos.module#PgNovosPageModule',canActivate:[AuthGuard] },
  { path: 'pg-antigos', loadChildren: './pages/pg-tabs/pg-antigos/pg-antigos.module#PgAntigosPageModule', canActivate: [AuthGuard] },
  { path: 'pg-nova-atividade', loadChildren: './pages/pg-nova-atividade/pg-nova-atividade.module#PgNovaAtividadePageModule', canActivate: [AuthGuard] },
  { path: 'pg-editar-atividade', loadChildren: './pages/pg-editar-atividade/pg-editar-atividade.module#PgEditarAtividadePageModule', canActivate: [AuthGuard] },
  { path: 'pg-editar-atividade/:id', loadChildren: './pages/pg-editar-atividade/pg-editar-atividade.module#PgEditarAtividadePageModule', canActivate: [AuthGuard] },
  { path: 'pg-editar-alunos', loadChildren: './pages/pg-editar-alunos/pg-editar-alunos.module#PgEditarAlunosPageModule', canActivate: [AuthGuard] },
  { path: 'pg-editar-professores', loadChildren: './pages/pg-editar-professores/pg-editar-professores.module#PgEditarProfessoresPageModule', canActivate: [AuthGuard] },
  { path: 'pg-detalhes-atividade', loadChildren: './pages/pg-tabs/pg-detalhes-atividade/pg-detalhes-atividade.module#PgDetalhesAtividadePageModule', canActivate: [AuthGuard] },
  { path: 'pg-detalhes-atividade/:id', loadChildren: './pages/pg-tabs/pg-detalhes-atividade/pg-detalhes-atividade.module#PgDetalhesAtividadePageModule', canActivate: [AuthGuard] },
  { path: 'pg-editar-atividades', loadChildren: './pages/pg-editar-atividades/pg-editar-atividades.module#PgEditarAtividadesPageModule', canActivate: [AuthGuard] },
  { path: 'pg-editar-usuarios', loadChildren: './pages/pg-editar-usuarios/pg-editar-usuarios.module#PgEditarUsuariosPageModule', canActivate: [AuthGuard] },
  { path: 'pg-editar-usuarios/:id', loadChildren: './pages/pg-editar-usuarios/pg-editar-usuarios.module#PgEditarUsuariosPageModule', canActivate: [AuthGuard] }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
