export interface Atividade{
    id?: string;
    data?: string; 
    hora?:string;
    titulo?: string; 
    disciplina?: string; 
    descricao?: string;
    imagemUrl?: string;
    usuarioId?: string;
    localizacao?:string;
}