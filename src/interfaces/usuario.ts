export interface Usuario {
     nome?: string;
     email?: string;
     curso?: string;
     perfil?: string;
     password?: string;
     imagemUrl?: string;
     usuarioId?: string;
}
