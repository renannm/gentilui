import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Atividade } from 'src/interfaces/atividades';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AtividadeService {
  private atividadeCollection: AngularFirestoreCollection<Atividade>;

  constructor(private afs: AngularFirestore) {
    this.atividadeCollection = this.afs.collection<Atividade>('Atividades');
  }

  getAtividades(){
    return this.atividadeCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;

          return { id, ...data };
        });
      })
    );

  }

  addAtividade( atividade: Atividade ){
    return this.atividadeCollection.add(atividade);
  }

  getAtividade( id: string ){
    return this.atividadeCollection.doc<Atividade>(id).valueChanges();
  }

  updateAtividade( id: string, atividade: Atividade ){
    return this.atividadeCollection.doc<Atividade>(id).update(atividade);
  }

  deleteAtividade( id: string){
    return this.atividadeCollection.doc(id).delete();
  }
}
