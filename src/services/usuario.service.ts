import { Injectable } from '@angular/core';
import { Usuario } from 'src/interfaces/usuario';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private usuarioCollection: AngularFirestoreCollection<Usuario>;

  constructor(
    private afs: AngularFirestore,) {
    this.usuarioCollection = this.afs.collection<Usuario>('Usuarios');
   }

   getUsuarios(){
    return this.usuarioCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;

          return { id, ...data };
        });
      })
    );

  }

  addUsuario( usuario: Usuario ){
    return this.usuarioCollection.add(usuario);
  }

  getUsuario( id: string ){
    return this.usuarioCollection.doc<Usuario>(id).valueChanges();
  }

  updateUsuario( id: string, usuario: Usuario ){
    return this.usuarioCollection.doc<Usuario>(id).update(usuario);
  }

  deleteUsuario( id: string){
    return this.usuarioCollection.doc(id).delete();
  }
}
