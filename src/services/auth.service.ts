import { Injectable } from '@angular/core';
import { Usuario } from 'src/interfaces/usuario';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public usuario: any = {}

  constructor(
    private afa: AngularFireAuth,
    private afs: AngularFirestore
    ) { }

  login(usuario: Usuario){
    return this.afa.auth.signInWithEmailAndPassword(usuario.email, usuario.password);
  }

  register(usuario: Usuario){
    return this.afa.auth.createUserWithEmailAndPassword(usuario.email, usuario.password);
  }

  logout(){
  return this.afa.auth.signOut();
  }

  getAuth(){
    return this.afa.auth;
  }
}
